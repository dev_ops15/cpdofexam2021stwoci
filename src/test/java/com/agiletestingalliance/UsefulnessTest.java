package com.qaagility.controller;
import static org.junit.Assert.*;
import org.junit.Test;

public class UsefulnessTest {
	@Test
	public void testDesc() throws Exception {
		String string1 = new String();
		assertEquals("Testing: Description", "DevOps is about transformation, about building quality in, improving productivity and about automation in Dev, Testing and Operations. <br> <br> CP-DOF is a one of its kind initiative to marry 2 distinct worlds of Agile and Operations together. <br> <br> <b>CP-DOF </b> helps you learn DevOps fundamentals along with Continuous Integration and Continuous Delivery and deep dive into DevOps concepts and mindset.", string1);
	}
/*
	@Test
        public void testWhileForLoops) throws Exception {
                GetString string2 = new String("HelloWorld!");
                assertEquals("Testing: String is null", "HelloWorld!", string2);
        }
*/
}
