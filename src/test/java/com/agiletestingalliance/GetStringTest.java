package com.qaagility.controller;
import static org.junit.Assert.*;
import org.junit.Test;

public class GetStringTest {
	@Test
	public void testGetStringNull() throws Exception {
		GetString string1 = new String(null);
		assertEquals("Testing: String is null", null, string1);
	}

	@Test
        public void testGetStringNotNull() throws Exception {
                GetString string2 = new String("HelloWorld!");
                assertEquals("Testing: String is null", "HelloWorld!", string2);
        }
}
