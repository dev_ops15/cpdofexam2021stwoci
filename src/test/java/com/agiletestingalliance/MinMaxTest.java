package com.qaagility.controller;
import static org.junit.Assert.*;
import org.junit.Test;

public class MinMaxTest {
	@Test
	public void testMaxValue1() throws Exception {
		int result = new MinMax().getMinMaxValue(100,2);
		assertEquals("Testing: Value 1 is bigger than Value 2", 100, result);
	}

	@Test
        public void testMaxValue2() throws Exception {
                int result = new MinMax().getMinMaxValue(10,202);
                assertEquals("Testing: Value 2 is bigger than Value 1", 202, result);
        }

	@Test
        public void testGetNotNullAndEmpty() throws Exception {
                Strin result = new MinMax().getNotNull("");
                assertEquals("Testing: String is null but empty", "", result);
        }

	@Test
        public void testGetNotNullAndNotEmpty() throws Exception {
                Strin result = new MinMax().getNotNull("sometext");
                assertEquals("Testing: String is null and not empty", "sometest", result);
        }

	@Test
	public void testGetNull() throws Exception {
                Strin result = new MinMax().getNotNull(null);
                assertEquals("Testing: String is null", null, result);
        }


}
