package com.agiletestingalliance;

public class MinMax {

	public int getMinMaxValue(int value1, int value2) {
		if (value2 > value1){
			return value2;
		}
		else{
			return value1; 
		}
	}
	
	public String getNotNull(String string) {
		//not null and empty
		if (string!=null && "".equals(string)){
			return string;
		}
		//not null and not emoty
		if (string!=null && !"".equals(string)){
			return string;
		}
		//is null
		if (string==null){
                        return string;
                }
		return string;
	}
	

}
